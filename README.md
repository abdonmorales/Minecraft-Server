[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://github.com/abdonmorales/Minecraft-Server)


# Paper Minecraft Server v1.4.3 (Paper 1.17.1 - build 104)
**Starter project enabling you a Paper Minecraft Server using just a Raspberry Pi 4, Jetson Nano, or really any computer.**

This project has been tested on a Raspberry Pi 4 B 4GB & Nvidia Jetson Nano. I do not recommend using a Raspberry Pi 3 or older. They have not enough RAM and power to calculate all the things. :boom:

## Why a Paper Minecraft Server?

* You can play anywhere. You can take the Pi to a friend, connect to his Wifi and enjoy playing. :video_game:
* It is free. No costs, no big server and no complication. :free:
* Why host on a computer? A Pi is power efficient! :rocket:
* You can easy maintain the files on the Pi by using your PC. :computer:

## Hardware required

* Raspberry Pi 4B (We recommend the 4GB or the 8GB model. 1GB is not enough!) or Nvidia Jetson Nano
* A fan or cooling system to prevent lag caused by throttling
* A 16GB or greater micro SD Card (We always recommend SanDisk Extreme Pro SD cards)
* Power supply

## Software required

* A download of this project (of course) or from our GitLab releases
* Linux or MacOS (if using shell script) *No Windows support yet
* Support for the following dependencies: wget and Java

## Setup and use :stars:

Run the shell script in the terminal:
`./mcsrv-init.sh` (the server will auto-start the jar, if there's an existing paper jar)

## Connect to another Wifi :satellite:

The Minecraft Server has wifi-connect integrated. You can use it for taking the Pi everywhere.

## Custom RAM (optional) :link:

Devices like the Raspberry Pi 4B 4GB or the 8GB model have enough RAM to run the server with more RAM (the default value used by balena Minecraft server is 1GB). If you set `RAM` to a value like `2G`, `4G`, or `6G` within the mcsrv-init.sh shell script for (Linux or MacOS).

## Add plugins (optional) :wrench:

Paper Minecraft (server) also supports plugins. Just drop the in the plugins folder using if you have the server on site. The current Minecraft version is `1.17.1`. You can get your plugins from there (Others work too.): 

* Spigot resources: https://www.spigotmc.org/resources/categories/spigot.4/
* Bukkit: https://dev.bukkit.org/bukkit-plugins

**NOTE:** Before adding the plugin and getting an error look if the plugin supports `1.17.1`.

## Play worldwide (optional) :earth_americas:

Once you’ve perfected the setup of your server on your local network, you might be interested in unveiling your server to the rest of the world! Here’s how you can enable remote access and allow players to connect via the Internet.

![NO-IP Picture](src/images/NO-IP.png)

If you’d like to allow friends outside of your local network to join your server, you’ll need to set up dynamic DNS (DDNS) to expose your Pi to the outside world. This example uses a service called No-IP, which has a free tier for people who want to try DDNS out, though other options and methods do exist as well. In the case of this example, you will need to: 

* Create an account with [No-IP](https://www.noip.com/sign-up) by visiting their website.
* After creating the account and logging in, create a Hostname (example: balena.serverminecraft.net) by [following their documentation](https://www.noip.com/support/knowledgebase/getting-started-with-no-ip-com/).
* Set up Port Forwarding: You will need to route your Minecraft traffic to port 25565 on your Pi. To do this, you will log in to your home router and setup Port Forwarding. This step varies by particular brand of modem or router, but the No-IP documentation does a good job of describing the process [here](https://www.noip.com/support/knowledgebase/general-port-forwarding-guide/). You may need to follow instructions specific to your modem or router if the No-IP documentation does not contain your particular type.
* Optional: You can login to No-IP with your router to keep the IP Address current in case it changes. That allows the router to connect automatically to No-IP. Here is a [guide by No-IP](https://www.noip.com/support/knowledgebase/how-to-configure-ddns-in-router/) on how to accomplish this.
* Paste your public / external internet address in the box labeled IP Address into the No-IP dashboard. You're done. 👍

For a deeper look at setting up remote access, please [reference this guide](https://www.noip.com/support/knowledgebase/getting-started-with-no-ip-com/) (Note: You can skip the DUC part).

## Custom Server (optional) :eyeglasses:

If you want to customize your server even further, but don't know where to start, take a look at some of the servers listed here for ideas:

* Spigot (Vanilla Java Edition): https://getbukkit.org/get/67c4d7b1f88726687c09137464efed99
* Purpur (Vanilla Java Edition, very well optimized for 1.17.1) https://purpur.pl3x.net/downloads/#1.17.1
* Paper (Vanilla Java Edition): https://papermc.io/downloads
* ccSpigot (Vanilla Java Edition, Fork and continuation of Paper 1.12.2): https://github.com/moom0o/ccSpigot


I encourage you to take your server build even further! There are many tutorials out there on server customization-- this article only touches on a few ideas. If you need help, please reach out by submitting an [issue on GitHub](https://github.com/abdonmorales/Minecraft-Server/issues).

This project is in active development so if you have any feature requests or issues please submit them here on GitLab. PRs are welcome, too. :octocat:
