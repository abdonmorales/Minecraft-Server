#!/bin/bash
echo "MC Server Initialization"
echo "Shell Script v1.4.3 (Paper)"

OS="`uname`"

MC_VERSION="1.17.1-104"

get_latest_server() {
  SERVER_JAR_URL="https://gitlab.com/abdonmorales/Minecraft-Server/-/raw/jars/paper-1.17.1-104.jar"
}

determine_OS() {
  if [[ OS="Darwin" ]]; then
   java -Xmx1G -Xms1G -jar paper-1.17.1-104.jar nogui
   echo "MacOS 11.X.X (Big Sur)"
   elif [[ OS="Linux" ]]; then
      exec java -Xmx1G -Xms1G -jar paper-1.17.1-104.jar nogui
      echo "Linux"
  fi
}

printf "%s" "Checking server JAR... "
  if [[ ! -e "paper-$MC_VERSION.jar" ]]; then
    printf "%s\n" "No server JAR found."
    get_latest_server
  fi


echo "Finished downloading PaperMC Server JAR 1.17.1-104 for MC Server (Paper) v1.4.3"

echo "JAR now up to date!"

echo "Now running JAR"

determine_OS
