#!/bin/bash
echo "Minecraft Server setup script!"
echo "This is for first time use only!"
echo "v1.4.3"
sleep 2
echo "Starting jar to setup (for first time use)"
mkdir ~/Server
git clone https://gitlab.com/abdonmorales/Minecraft-Server.git
git switch jars
mv mcsrv-init.sh ~/Server
cp paper-1.17.1-104.jar ~/Server
cd ~/Server
java -jar paper-1.17.1-104.jar

echo "Replace false with true in eula.txt and then run the second shell script 'mcsrv-init.sh' with in the 'Server' folder"
